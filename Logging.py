import logging

logging.debug('debug')
logging.info('info')
logging.warning('warning')
logging.error('error')
logging.critical('critical')

WARNING: logging.warning
ERROR: logging.error
CRITICAL: logging.critical

logging.basicConfig(
    filename='debug.log',
    filemode='w',
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%d.%m.%y %H:%M:%S',
    level=logging.DEBUG
)

import socket
    socket.socket (socket.AF_INET,
                      socket.SOCK_DGRAM)
                      
    UPD_IP = " "
    UPD_PORT = 5005
    MESSAGE = "debug.log"